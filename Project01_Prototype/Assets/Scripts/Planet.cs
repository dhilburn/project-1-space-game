﻿using UnityEngine;
using System.Collections;

/**
 * @author Darrick Hilburn
 * 
 * This class sets up the foundations for creating a 
 * planet in this game.
 * 
 * @param parentPlanet  - Planet a planet rotates around
 * @param orbitSpeed    - Speed at which a planet rotates the parent
 * @param rotationSpeed - Speed at which a planet rotates on its axis
 * @param diameter      - Planet diameter
 * @param mass          - Planet mass
 * @param sunDistance   - Distance between edges of the parent planet and this planet
 */
[ExecuteInEditMode]
public class Planet : MonoBehaviour
{
    #region Sun Vars
    [Tooltip("Speed this planet rotates on its axis at. Measured in days")]
    [Range(-250, 100)]
    public float rotationSpeed;

    [Tooltip("Diameter of a planet. Measured in Mm/10 (10^5 meters)")]
    [Range(0.1f, 15f)]
    public float diameter;

    [Tooltip("Mass of a planet. Measured in EM (Earth Masses)")]
    [Range(0.01f, 350f)]
    public float mass;
    #endregion

    #region Moon Vars
    [Tooltip("Planet that this planet orbits.")]
    [SerializeField]
    GameObject parentPlanet;

    [Tooltip("Distance between the edge of this planet to its parent's edge. Measured in AU (Astronomical Units)")]
    public float parentDistance;

    [Tooltip("Speed this planet orbits its parent at. Measured in EY (Earth Years)")]
    [Range(-50, 50)]
    public float orbitSpeed;    


    #endregion

    #region Planet Vars
    [Tooltip("Moons a planet has.")]
    public GameObject[] moons = new GameObject[3];
    #endregion



	void Start () 
    {
        InitializeRadius();
        InitializePosition();
	}
	
	void Update ()
    {
        // Show change in planet size in the Unity editor
#if UNITY_EDITOR
        InitializeRadius();
#endif
        Rotate();
        Orbit();
    }

    #region Sun Functions
    // InitializeRadius sets the planet to its size
    void InitializeRadius()
    {
        Vector3 size = new Vector3(diameter, diameter, diameter);
        gameObject.transform.localScale = size;
    }

    // Rotate rotates a planet on its axis
    void Rotate()
    {
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }

    #endregion

    #region Moon Functions
    //  InitializePosition sets up an initial position for the planet based on
    //  the edge of the parent and this planet.
    void InitializePosition()
    {
        Vector3 parentPos = parentPlanet.transform.position;
        float parentRadius = GetRadius(parentPlanet);
        float posOffset = parentRadius + parentDistance + diameter / 2;
        Vector3 thisPosition = parentPos + new Vector3(0, 0, posOffset);
        transform.position = thisPosition;
    }

    // GetRadius calculates the average radius of a planet using its diameter
    float GetRadius(GameObject sentObject)
    {
        return (sentObject.transform.localScale.x + sentObject.transform.localScale.y + sentObject.transform.localScale.z) / 3 / 2;
    }

    // Orbit orbits a planet around its parent planet
    void Orbit()
    {
        transform.RotateAround(parentPlanet.transform.position, Vector3.up, orbitSpeed * Time.deltaTime);
    }
    #endregion

    
      
    

    
    

    

    
}